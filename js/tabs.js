(function($) {

  $(window).load(function() {
    if ($(window).width() <= '539') {
      $('.point_img').attr('src', 'images/point_img_mobile.jpg');
      $('.point_img').css('max-width', '320px');
      $('.point_img').css('left', '50%');
      $('.point_img').css('margin-left', '-160px');
    } else {
      $('.point_img').attr('src', 'images/point_img.jpg');
      $('.point_img').css('width', '100%');
    }
    $(".price-list-cat").mCustomScrollbar({
      theme: "rounded-dots-dark",
      advanced: {
        updateOnContentResize: false,
        updateOnImageLoad: false
      },
      axis: "yx",
      //scrollbarPosition: "outside",

      // autoHideScrollbar: true,

      scrollInertia: 100

    });
  });

  $('#menu_cat').click(function() {
    if ($('.tabs ul.list').is(':hidden')) {
      $('.tabs ul.list').slideDown('slow');
      $('#menu_cat img').css('transform', 'none');
    } else {
      $('.tabs ul.list').slideUp("slow");
      $('#menu_cat img').css('transform', 'rotate(-180deg)');
    }

  });

  jQuery.fn.lightTabs = function(options) {

    var size_array = ['armatura', 'kvadrat_stall_1', 'truba_profil', 'shveler', 'list_holodn', 'setka_1', 'profnastil', 'provolka_vyaz_1', 'krug_stall_1', 'truba_kruglaya_1', 'ugolok', 'polosa_stall', 'setka_2'];

    var createTabs = function() {
      tabs = this;
      i = 0;

      showPage = function(i) {
        //$(tabs).children("div").children("div").children("div").hide();
        //$(tabs).children("div").children("div").children("div").eq(i).show();
        // $(tabs).children("ul").children("li").removeClass("active");
        // $(tabs).children("ul").children("li").eq(i).addClass("active");
        $('#catalog_front').animate({
          'opacity': '1'
        }, 20, function() {
          $('#catalog_front').css('background-image', 'url(images/cat/' + size_array[i] + '.jpg)');
          //alert($(tabs).children("ul").children("li").eq(i).text());
          var retext = $(tabs).children("ul").children("li").eq(i).text();
          var data_cat = $(tabs).children("ul").children("li").eq(i).attr('data-cat');
          /*$.ajax({
            url: './ajax_catalog.php',
            data: {
              id: data_cat
            },
            type: 'POST',
            headers: {
              "cache-control": "no-cache"
            },
            dataType: "json",
            async: false,
            success: function(data) {
              //alert(data);
              //alert(data.length);
              if (!data) {
                $('#new_error ul').append('<li>Попробуйте позднее</li>');
              } else if (data.length == 0) {
                //alert('ok');
              } else {

                // alert(data);
                //alert(str);
                if ($('#mCSB_1_container').length) {
                  $('#mCSB_1_container').html(data[0]);
                  $(".price-list").mCustomScrollbar("update");
                  $('#menu_cat').html(retext + '<img src="arrow-cat.png" />');
                  $(".round1 span").html(data[1]);
                  $(".round2 span").html(data[2]);
                  $(".round3 span").html(data[3]);
                } else {

                  $("#price-list").html(data[0]);
                  $('#menu_cat').html(retext + '<img src="arrow-cat.png" />');
                  $(".round1 span").html(data[1]);
                  $(".round2 span").html(data[2]);
                  $(".round3 span").html(data[3]);
                }
              }
            }
          });*/

          return false;

        });

      }

      showPage(0);

      $(tabs).children("ul").children("li").each(function(index, element) {
        $(element).attr("data-page", i);
        i++;
      });

      $(tabs).children("ul").children("li").click(function() {
        var bg = $('#catalog_front').css('background-image');
        //bg = bg.replace('url(','').replace(')','');
        //alert($(this).text());
        var poscat = $(this).text();
        $('#poscat').val(poscat);
        //$('#menu_cat').html(poscat+'<img src="arrow-cat.png" />');
        $('#catalog_back').css('background-image', bg);
        $('#catalog_front').animate({
          'opacity': '0.6'
        }, 90);
        showPage(parseInt($(this).attr("data-page")));
        if ($(window).width() <= '768')
          $('.tabs ul.list').delay(1000).slideUp("slow");
        else {}
      });
    };
    return this.each(createTabs);
  };

})(jQuery);
$(document).ready(function() {
  $(".tabs").lightTabs();
  $("a.example2").fancybox({
    padding: 0,
    //openEffect : 'elastic',
    openEffect: 'fade',
    openSpeed: 300,
    //closeEffect : 'elastic',
    closeEffect: 'fade',
    closeSpeed: 300,
    closeClick: true,
    helpers: {
      overlay: null
    }
  });
  $('#map_over').click(function() {
    $(this).css('z-index', '-1');
  });
});



// $(document).scroll(function(){
// var s = $(window).scrollTop();
// var angle = 0;
//     setInterval(function(){
//           angle+=3;
//          jQuery("#rotateImg").rotate(angle);
//     },50);
// $(".gear1,.gear3,.gear5").css({"-moz-transform": "rotate("+angle+"deg)",
//             "-ms-transform": "rotate("+angle+"deg)",
//             "-webkit-transform": "rotate("+angle+"deg)",
//             "-o-transform": "rotate("+angle+"deg)",
//             "transform": "rotate("+angle+"deg)",});

$(document).scroll(function() {
  var s = $(window).scrollTop();
  $(".gear1,.gear3,.gear5").css({
    "-moz-transform": "rotate(" + s / 10 + "deg)",
    "-ms-transform": "rotate(" + s / 10 + "deg)",
    "-webkit-transform": "rotate(" + s / 10 + "deg)",
    "-o-transform": "rotate(" + s / 10 + "deg)",
    "transform": "rotate(" + s / 10 + "deg)",
  });
  $(".gear2,.gear4").css({
    "-moz-transform": "rotate(" + (-s / 10) + "deg)",
    "-ms-transform": "rotate(" + (-s / 10) + "deg)",
    "-webkit-transform": "rotate(" + (-s / 10) + "deg)",
    "-o-transform": "rotate(" + (-s / 10) + "deg)",
    "transform": "rotate(" + (-s / 10) + "deg)",
  });
});
