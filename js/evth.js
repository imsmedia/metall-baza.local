(function(){
  'use strict';
  var date = new Date();
  var currentDateString = [];
  currentDateString.push(date.getDate() < 10 ? '0' + date.getDate() : date.getDate());
  currentDateString.push(parseInt(date.getMonth())+1 < 10 ? '0' + (parseInt(date.getMonth())+1) : (parseInt(date.getMonth())+1));
  currentDateString.push(date.getFullYear());
  $('#current-date').html(currentDateString.join('.'));
  var App = window.App = {
    Models: {},
    Collections: {},
    Views: {},
    Data: window.data,
    Events: new Model()
  };
  var tree = new Tree(App.Data.categories);
  var needArr = [
    850, 10, 12, 62, 11, 25, 337, 853, 9, 103, 19, 40, 109
  ];
  var advantages = [
    ['любой размер', 'спец предложения', 'минимальная цена'],
    ['порезка в любой размер', 'минимальная цена', 'бесплатная погрузка'],
    ['минимальная цена', 'бесплатная порезка', 'продаем и в метрах и в тоннах'],
    ['бесплатная порезка', 'быстрое оформление заказа', 'порезка в размер даже при заказе по телефону'],
    ['бесплатная погрузка', 'быстрое оформление заказа', 'спец предложения'],
    ['минимальная цена', 'быстрое оформление заказа', 'бесплатная погрузка'],
    ['бесплатная доставка от<br19 999', 'быстрое оформление заказа', 'спец предложения'],
    ['минимальная цена', 'быстрое оформление заказа', 'спец предложения'],
    ['бесплатная порезка', 'минимальная цена', 'порезка в размер даже при заказе по телефону'],
    ['бесплатная доставка от<br>19 999', 'бесплатная погрузка', 'точный вес'],
    ['порезка в размер даже при заказе по телефону', 'минимальная цена', 'спец предложения'],
    ['бесплатная погрузка', 'точный вес', 'порезка в любой размер'],
    ['минимальная цена', 'бесплатная погрузка', 'быстрое оформление заказа']
  ];
  var list = tree.domTree(needArr);
  list.className = 'list';
  list.addEventListener('click', function(e){
    var self = this;
    function findIndex(target) {
      var lis = self.querySelectorAll('li');
      for (var i = 0; i < lis.length; i++) {
        if (lis[i]===target) return i;
      }
    };
    var active = this.querySelector('.active');
    if (active) active.className="";
    var target = e.target;
    if (target.nodeName !== 'LI') return;
    target.className = 'active';
    var attr = JSON.parse(target.getAttribute('data-cats'));
    var index = findIndex(target);
    var rounds = document.querySelectorAll('.round span');
    for (var i = 0; i < rounds.length; i++) {
      rounds[i].innerHTML = advantages[index][i];
    }
    if (attr.length===1) {
      var products = _.filter(App.Data.products, function(product){
        return product.category_id === attr[0];
      });
    } else {
      var productsCol = [];
      for (var i = 0; i < attr.length; i++) {
        productsCol.push(_.filter(App.Data.products, function(product){
          return product.category_id === attr[i];
        }));
      };
      var products = []
      for (var i = 0; i < productsCol.length; i++) {
        products = products.concat(productsCol[i]);
      }
    }
    var prodsCol = new Backbone.Collection(products);
    var prodsView = new App.Views.Products({collection: prodsCol});
    prodsView.render();
  })
  $('#menu_cat').after(list);
  setTimeout(function(){
    list.querySelector('li').click();
  }, 1000);
  // App.Views.Cat = Backbone.View.extend({
  //   tagName: 'li',
  //   events: {
  //     'click' : 'show',
  //   },
  //   initialize: function(){
  //     this.$el.html(this.model.get('name'));
  //     return this;
  //   },
  //   show: function(){
  //     this.showProducts();
  //     this.showName();
  //     this.$el.parent().find('.active').removeAttr('class');
  //     this.$el.addClass('active');
  //   },
  //   showProducts: function(){
  //     var self = this;
  //     var products = _.filter(App.Data.products, function(product){
  //       return product.category_id === self.model.get('id')
  //     });
  //     var prodsCol = new Backbone.Collection(products);
  //     var prodsView = new App.Views.Products({collection: prodsCol});
  //     prodsView.render();
  //   },
  //   showName: function(){
  //     $('#cat-name').html(this.model.get('name'));
  //   }
  // });
  // App.Views.Cats = Backbone.View.extend({
  //   collection: cats,
  //   tagName: 'ul',
  //   id: 'cats-list',
  //   className: 'list',
  //   initialize: function(){
  //     this.collection.models = this.collection.sortBy('name');
  //   },
  //   render: function(){
  //     this.collection.each(function(model) {
  //       var view = new App.Views.Cat({model: model});
  //       this.appendItem(view);
  //     }, this);
  //     $('#menu_cat').after(this.el);
  //   },
  //   appendItem: function(view){
  //     this.$el.append(view.el)
  //   },
  //
  // });
  App.Views.Product = Backbone.View.extend({
    tagName: 'tr',
    template: _.template('<td><%= name %></td><td><%= price ? price.toFixed(2) : "Договорная" %></td>'),
    initialize: function(param){
      var measureId = this.model.get('measure');
      var basicMeasureId = this.model.get('basic_measure');
      if (measureId)
        this.model.set('measure', App.Data.measures.filter(function(el){return el.id===measureId;})[0].name);
      if (basicMeasureId)
        this.model.set('basic_measure', App.Data.measures.filter(function(el){return el.id===basicMeasureId;})[0].name);
      this.render(param);
    },
    render: function(param){
      if (param.template) {
        this.template = _.template(param.template);
      }
      this.$el.html(this.template(this.model.toJSON()));
      return this;
    }
  });
  App.Views.Products = Backbone.View.extend({
    tagName: 'tbody',
    initialize: function(){

    },
    render: function(param){
      var singleTemplate = false;
      if (param) {
        singleTemplate = param.singleTemplate || false;
      }
      this.collection.each(function(model) {
        var view = new App.Views.Product({model: model, template: singleTemplate});
        this.appendItem(view);
      }, this);
      if (!param) {
        $('.catalog tbody').html('');
        $('.catalog').append(this.el);
        // $(".price-list").mCustomScrollbar();
        $(".price-list").mCustomScrollbar("update");
      } else {
        if (param.slider) {
          $(param.selector).html('');
          $(param.selector).append(this.el);
        }
      }

    },
    appendItem: function(view){
      this.$el.append(view.el)
    },
  });
  // var cats = new Backbone.Collection(App.Data.categories);
  // var catView = new App.Views.Cats({collection: cats});
  // catView.render();
  var campaings = {
    'armatura': [850],
    'dvutavr': [215],
    'krug': [9],
    'kvadrat_stal': [10],
    'list_holodnokatan': [851],
    'profnastil': [337],
    'provoloka': [853],
    'setka': [852],
    'shveller': [62],
    'stal_polosa': [40],
    'truba_kruglaya': [103],
    'truba_profil': [12],
    'ygolok_stal': [19],
    'setka_svarnaya': [25, 32]
  };
  var utm = unserializeUrl();
  if (utm.hasOwnProperty('utm_campaign')) {
    var val = utm['utm_campaign'];
    var index = val.indexOf('_poisk');
    if (index!==-1) {
      val = val.substr(0, index);
      utm['utm_campaign']=val;
    }
  }

  for (var key in campaings) {
    if (!campaings.hasOwnProperty(key)) continue;
    App.Events.on(key, function(data){
      var childs = [];
      for (var i = 0; i < data.length; i++) {
        var id = data[i];
        childs.push(tree.getAllChilds(id));// array of arrays
      }
      childs.forEach(function(el){
        var ids = [];
        if (el.length > 1) {
          ids = el.slice(1);
        } else {
          ids = el;
        }
        for (var i = 0; i < ids.length; i++) {
          var crEl = document.createElement.bind(document);
          var cat = tree.find(ids[i]);
          var div = crEl('div');
          div.innerHTML = cat.name;
          (function(id) {
            div.addEventListener('click', function(){
              var products = _.filter(App.Data.products, function(product){
                return product.category_id === id /*&& product.price!==0*/;
              });
              var prodsCol = new Backbone.Collection(products);
              var prodsView = new App.Views.Products({collection: prodsCol});
              prodsView.render({
                selector: '.evth__products-container',
                slider: true,
                singleTemplate: '<div><%= name %> --- measure - <%= measure %> --- <%=price %> --- basic_measure - <%= basic_measure %></div>'
              });
            });
          })(cat.id);
          $('.evth').prepend(div);
        }
      })
    });
  };
  // console.log(utm);
  App.Events.callEvent(utm['utm_campaign'], campaings[utm['utm_campaign']]);
})()
