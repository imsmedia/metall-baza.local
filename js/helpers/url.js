function unserializeUrl(){
  var pairs = window.location.search;
  if (window.location.search=='') return '';
  var temp;
  var obj = {};
  pairs = pairs.substring(1).split('&');
  for (var i = 0; i < pairs.length; i++) {
    temp = pairs[i].split('=');
    obj[temp[0]] = temp[1];
  }
  return obj;
}
